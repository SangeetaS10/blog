class FixColumnName < ActiveRecord::Migration
  def change
    rename_column :comments, :postid ,:post_id
  end
end
